#!/usr/bin/python3

import requests
import json
import yaml
import os

# exceptions:
class WordNotFound(Exception):
    pass
class SearchedWordNotFound(Exception):
    pass
class InternetError(Exception):
    pass
class InternetTimeout(Exception):
    pass

# load config
config = yaml.safe_load(open(os.path.expanduser("~/.config/oxplainrc")))
TIMEOUT = config["timeout"] if "timeout" in config else 5

# overwrite class abbreviations
if 'abrclass' in config:
    abrclass = config[abrclass]
else:
    abrclass = {
            'noun': 'n',
            'adjective': 'adj',
            'pronoun': 'pro',
            'numeral': 'num',
            'verb': 'v',
            'adverb': 'adv',
            'preposition': 'pre',
            'conjunction': 'conj',
            'determiner': 'det',
            'article': 'article',
            'interjection': 'interj',
            'residual': 'res'
    }

def get(language, word, debug=False):
    url = 'https://od-api.oxforddictionaries.com/api/v2/entries/'  + language + '/'  + word.lower()
    #url Normalized frequency
    # urlFR = 'https://od-api.oxforddictionaries.com:443/api/v2/stats/frequency/word/'  + language + '/?corpus=nmc&lemma=' + word_id.lower()
    try:
        r = requests.get(url, headers = {'app_id' : config['app_id'], 'app_key' : config['app_key']}, timeout=TIMEOUT)
    except requests.exceptions.ConnectionError:
        raise InternetError
    except requests.exceptions.Timeout:
        raise InternetTimeout
    
    if debug:
        print(r.text)
        return {}

    data = r.json()
    parsed = {}
    
    if 'error' in data:
        raise WordNotFound

    # {id:, language:, lexicalEntries:[by word class], type:headword}
    result_len = len(data['results'])
    if result_len == 1:
        data = data['results'][0]['lexicalEntries']
    else:
        data = [ x[0] for x in [ x['lexicalEntries'] for x in data['results'] ] ]

    # different word classes
    for dwc in data:
        # {derivatives[], entries[], lexicalCategory{}, phrases[], pronunciations[]}      
        wclass = dwc['lexicalCategory']['id']  # word class

        
        entry = dwc['entries'][0]  # always only one element
        etymology = entry['etymologies'][0] if 'etymologies' in entry else False
        note = entry['notes'][0]['text'] if 'notes' in entry else False
    
        parsed[wclass] = {
                'class': wclass,
                'sclass': abrclass[wclass],
                'etymology': etymology,
                'note': note,
                'senses': []
        }

        if 'pronunciations' in dwc:
            # pronuniation at this level only when 1 result, else deeper
            # if result_len == 1:
            parsed[wclass]['pronunciation'] = dwc['pronunciations'][0]['phoneticSpelling']
            # print(dwc['pronunciations'][0])
        elif 'pronunciations' in entry:
            parsed[wclass]['pronunciation'] = entry['pronunciations'][0]['phoneticSpelling']
        elif 'pronunciation' in entry:
            parsed[wclass]['pronunciation'] = entry['pronunciations'][0]['phoneticSpelling']
        else:
            parsed[wclass]['pronunciation'] = False # hew

        # different senses, meanings
        for sense in entry['senses']:
            definition = sense['definitions'][0]
            short_definition = sense['shortDefinitions'][0] if 'shortDefinitions' in sense else False
            example = sense['examples'][0]['text'] if 'examples' in sense else False
            synonyms = [ syn['text'] for syn in sense['synonyms'][:3] ] if 'synonyms' in sense else False # max 3 synonyms
        
            parsed[wclass]['senses'].append({
                    'def': definition,
                    'sdef': short_definition,
                    'ex': example,
                    'syn': synonyms
            })

    return parsed
