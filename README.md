% OXPLAIN(1) oxplain man page | Version 1.0
% Tomáš Faikl (tomasfaikl@seznam.cz)
% 19-August-2020

# `oxplain` - explain a word using Oxford Dictionary 

```
oxplain [-h] [-l language] [-p] [-d] word
```

# Options

**-d**, **--debug**
display a raw JSON response from Oxford Dictionary API

**-l** _language_, **--lang** _language_
specify source language of _word_;
default english

**-p**, **--prefix**
print number prefixes (starting with zero),
prepends each line with a number linking sense to a word class;
use for ex. with **dmenu-oxplain**; 
see below for examples

**-h**, **--help**
display help and exit

# Description

`oxplain` is a Python wrapper around Oxford Dictionary API

Outputs all word classes (noun, verb, ...) for a given _word_ with short definitions of different senses for said word class,
following with a longer definition for each sense, accompanied with up to three synonyms of given sense and an example.
At the end will be printed etymology (Origin) and a Note.

These additional features (synonyms, examples, etymology and notes) need not be available in the Oxford Dictionary for all words.

There is also a man page included.

# User interface `dmenu-oxplain`
`oxplain` comes with a convenient & truly minimal UI for `dmenu` (a minimalist menu for Unix) which will every Unix nerd LOVE (written in POSIX sh)

## Features
- word completion using an offline wordlist (see below for source)
- convenient Oxford Dictionary output display; select a word class for given word to discover possible sences of the word (for this class)
- easy & intuitive usage
- and mainly:

### vocabulary builder & trainer
- creation of multiple vocabularies based on your **search history**
- select the meaning you were looking for and an editor will popup
- here you can enter translations to other languages or meaning of the word written more concisely
- data will be saved to multiple dictionaries in structured format (default is "key";"value")
- this can be used to feed data to **vocabulary trainer** (for ex. [Android Vocabulary Trainer](#))

Also comes with duplicate word conflict resolver

#### vocabulary files
Vocabulary files are located in `$OXPLAINDIR` directory. If it is not set then this is set to `$XDG_DATA_DIR/oxplain` or `$HOME/.local/share/oxplain` if `$XDG_DATA_HOME` is not set. Here are two files (`1` and `2`) which indicate the two dictionaries. Each is structured in `"key";"value"` format (see below for examples).

I personally use `1` for english-english training and `2` for english-czech (my native language) training.

### Requirements for `dmenu-oxplain`
1. Have `dmenu` installed (it should be available as a package in you package manager {if you do not know what that is, type `sudo apt-get update && sudo apt-get install dmenu`})
2. Set your `$TERMINAL` and `$EDITOR` variables to your terminal emulator program and text editor respectively. Setting of `$TERMINAL` is done so that you can invoke it via a keyboard shortcut for example.
	1. this can be done either in your `~/.profile` (`export TERMINAL=st; export EDITOR=vim`) + logout- **prefered**
	2. or inline when calling the script: `TERMINAL=st EDITOR=vim ./dmenu-oxplain` (mine terminal is suckless' `st`, look it up, it's good)

# INSTALLATION

## Configuration
Create file `~/-config/oxplainrc` with following content (YAML):
```yaml
app_id: "<YOUR OXFORD DICTIONARY API ID HERE>"
app_key": "<YOUR OXFORD DICTIONARY API KEY HERE>"

# optional
# specify internet connection timeout, useful for dmenu-oxplain UI
timeout: 5
```
Oxford Dictionary keys can be acquired for free (1000 queries/month) at Oxford Dictionary Developer [API registration (Prototype)](https://developer.oxforddictionaries.com/#plans)

## Adding to PATH
If you want to add `oxplain` or `dmenu-oxplain` to PATH, then create a file with same name in a directory which is already in PATH ("~/.local/bin" is proffered for user installations but is not in path by default).

Place following content to the file:
```sh
#!/bin/sh
cd "<YOUR OXPLAIN DIRECTORY>" && ./dmenu-oxplain "$@" # or replace with ./oxplain "$@"
```
where `<YOUR OXPLAIN DIRECTORY> is the folder where you downloaded this repository.

# EXAMPLES

`oxplain -p queer`
```
0 /kwɪə/ adj.: strange | homosexual
       0 strange; odd | odd, strange, unusual | she had a queer feeling that they were being watched
       0 (of a person) homosexual. | gay, lesbian, lesbigay
1 /kwɪə/ n.: homosexual man
        1 a homosexual man. | gay person, lesbian, gay

2 /kwɪə/ v.: spoil or ruin
        2 spoil or ruin (an agreement, event, or situation) | spoil, damage, impair | Reg didn't want someone meddling and queering
the deal at the last minute

O early 16th century: considered to be from German quer‘oblique, perverse’, but the origin is doubtful
N The word queer was first used to mean ‘homosexual’ in the late 19th century; when used by heterosexual people, it was originally an aggressively derogatory term. By the late 1980s, however, some gay people began to deliberately use the word queer in place of gay or homosexual, in an attempt, by using the word positively, to deprive it of its negative power. Queer also came to have broader connotations, relating not only to homosexuality but to any sexual orientation or gender identity not corresponding to heterosexual norms. The neutral use of queer is now well established and widely used, especially as an adjective or noun modifier, and exists alongside the derogatory usage
```

`oxplain evanesce`
```
/ˌiːvəˈnɛs/ v.: pass out of sight or existence
        pass out of sight, memory, or existence | decrease, decline, diminish | water moves among reeds, evanesces, shines

O mid 19th century: from Latin evanescere, from e- (variant of ex-) ‘out of’ + vanus‘empty’
```


Vocabulary file example (first column is the key word, second is custom text):
```
"unquenchable";"uncontrolled, not constrained"
"valour";"fearlessness"
"reverence";"high regard, respect"
"sundered";"split apart"
"shun";"avoid, evade"
"cleft";"divided into two"
"caress";"gentle/loving touch, stroke"
"cower";"crouch in fear"
"anguish";"agony, torment"
"firmament";"dome, arch"
"ragged";"torn-clothed, exhausted"
"garment";"item of clothing"
"assail";"violent attack"
"tarry";"stay longer than intended / covered with"
```

# Exit Codes

**1**: Word was not found in Oxford Dictionary. You can try again with a more basic form of the word.


**2:** Internet connection problems,


**10**: processing error, unexpected API output, a bug for the moment. Please, fill in a bug report with give word.

dmenu interface **dmenu-oxplain** shares the same exit codes.


# SEE ALSO

**dmenu-oxplain**
A dmenu wrapper written in (POSIX) shell script uses this to provide an interactive interface. Select class to display subsenses or select etymology/note and display it in $EDITOR.

**English words list**
[https://github.com/dwyl/english-words](https://github.com/dwyl/english-words)

**GNU Collaborative International Dictionary of English**
Totally free (also possibly offline) alternative to Oxford Dictionary, although it is not so detailed and elabored.
[https://gcide.gnu.org.ua](https://gcide.gnu.org.ua)

**WordNet from Princeton University**
Free (according to license even for commercial use) (offline&online) dictionary with special db format. Link to command line tool to process the database.
[https://wordnet.princeton.edu/documentation/wn1wn](https://wordnet.princeton.edu/documentation/wn1wn)

# BUGS
There is ocassionally a word which has a little different Oxford Dictionary output; please submit a bug report with the word that caused the problems.

See file `bug_words`.


# Author
Tomáš Faikl (tomasfaikl@seznam.cz)
